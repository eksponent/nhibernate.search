NHibernate.Search (forked)
================================

This is NHibernate.Search ripped out from [NHContrib](http://sourceforge.net/projects/nhcontrib/) and updated to recent version of NHibernate.

  * Removed NAnt build scripts
  * Ignored a couple of concurrency tests that I couldn't fix.
  * Replaced lib directory with restorable NuGet packages

I acknowledge that this should probably be hosted somewhere else, and that it might NOT work in your software.

This is NOT a release associated with NHForge but just an attempt to scratch a local itch ;)

